;;; dashboard-widgets.el --- A startup screen extracted from Spacemacs  -*- lexical-binding: t -*-

;; Copyright (C) 2022 Rafał Rączka <info@rafalraczka.com>

;; Author: Rafał Rączka <info@rafalraczka.com>
;; URL: https://github.com/emacs-pico-dashboard/emacs-pico-dashboard

;; This file is not part of GNU Emacs.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;; Package-Version: 0.0.1-SNAPSHOT
;; Keywords: startup, screen, tools, dashboard
;; Package-Requires: ((emacs "26.1"))

;;; Commentary:

;; Minimal Emacs dashboard.

;;; Code:

(require 'subr-x)
(require 'image)

(defcustom pico-dashboard-item-key-separator " "
  "Separator to use between item and key."
  :type 'string
  :group 'pico-dashboard)

(defcustom pico-dashboard-items-separator "\n"
  "Separator to use between items."
  :type 'string
  :group 'pico-dashboard)

(defcustom pico-dashboard-key-wrappers '("[" "]")
  ""
  :type 'list
  :group 'pico-dashboard)

(defcustom pico-dashboard-page-separator "\n\n"
  "Separator to use between sections."
  :type 'string
  :group 'pico-dashboard)

(defcustom pico-dashboard-image-banner-max-height 0
  "Maximum height of banner image.
This setting applies only if Emacs supports image transforms or
compiled with Imagemagick support.  When value is non-zero the
image banner will be resized to the specified height in pixels,
with aspect ratio preserved."
  :type 'integer
  :group 'pico-dashboard)

(defcustom pico-dashboard-image-banner-max-width 0
  "Maximum width of banner image.
This setting applies if Emacs supports image transforms or
compiled with Imagemagick support.  When value is non-zero the
image banner will be resized to the specified width in pixels,
with aspect ratio preserved."
  :type 'integer
  :group 'pico-dashboard)

(defcustom pico-dashboard-format t
  "Pico-Dashboard format."
  :type 'boolean
  :group 'pico-dashboard)

(defcustom pico-dashboard-footer-messages
  '("Happy coding!")
  "A list of messages, one of which pico-dashboard chooses to display."
  :type 'list
  :group 'pico-dashboard)

(defcustom pico-dashboard-set-footer nil
  "When non nil, a footer will be displayed at the bottom."
  :type 'boolean
  :group 'pico-dashboard)

(defcustom pico-dashboard-show-shortcuts t
  "Whether to show shortcut keys for each item."
  :type 'boolean
  :group 'pico-dashboard)

(defconst pico-dashboard-banners-directory
  (concat (file-name-directory (locate-library "pico-dashboard")) "banners/")
  "Default banner directory.")

(defconst pico-dashboard-banner-classic-png
  (concat pico-dashboard-banners-directory "classic.png")
  "Emacs banner image.")

(defconst pico-dashboard-banner-logo-png
  (concat pico-dashboard-banners-directory "logo.png")
  "Emacs banner image.")

(defconst pico-dashboard-banner-length 75
  "Width of a banner.")

(defcustom pico-dashboard-banner-logo-title "Welcome to Emacs!"
  "Specify the startup banner."
  :type 'string
  :group 'pico-dashboard)

(defcustom pico-dashboard-footer
  (nth (random (1- (1+ (length pico-dashboard-footer-messages)))) pico-dashboard-footer-messages)
  "A footer with some short message."
  :type 'string
  :group 'pico-dashboard)

(defcustom pico-dashboard-startup-banner 'classic
  "Specify the startup banner.
Default value is `classic', it displays the Emacs logo.  `logo'
displays Emacs alternative logo.  An integer value is the index
of text banner.  A string value must be a path to a .PNG or .TXT
file.  If the value is nil then no banner is displayed."
  :type '(choice (const :tag "classic" classic)
                 (const :tag "logo" logo)
                 (const :tag "ascii" ascii)
                 (symbol :tag "other built-in")
                 (string :tag "a png or txt path"))
  :group 'pico-dashboard)

(defcustom pico-dashboard-buffer-last-width nil
  "Previous width of pico-dashboard-buffer."
  :type  'integer
  :group 'pico-dashboard)

(defcustom pico-dashboard-items-justify t
  ""
  :type 'boolean
  :group 'pico-dashboard)

(defcustom pico-dashboard-items nil
  "Association list of items to show in the startup buffer.
Will be of the form `(list-type . list-size)'.  If nil it is
disabled.  Possible values for list-type are: `recents'
`bookmarks' `projects' `agenda' `registers'."
  :type  '(repeat (alist :key-type symbol :value-type integer))
  :group 'pico-dashboard)

(defvar pico-dashboard-buffer-name)

;;;; Faces

(defface pico-dashboard-banner-logo-title
  '((t :inherit default))
  "Face used for the banner title."
  :group 'pico-dashboard)

(defface pico-dashboard-footer
  '((t (:inherit font-lock-doc-face)))
  "Face used for widget headings."
  :group 'pico-dashboard)

(defface pico-dashboard-items
  '((t (:inherit default)))
  "Face used for items."
  :group 'pico-dashboard)

(defface pico-dashboard-keys
  '((t (:inherit shadow)))
  "Face used for shortcuts for items."
  :group 'pico-dashboard)

(defface pico-dashboard-text-banner
  '((t (:inherit font-lock-keyword-face)))
  "Face used for text banners."
  :group 'pico-dashboard)

;;;; Generic widget helpers

(defun pico-dashboard-append (msg &optional _messagebuf)
  "Append MSG to dashboard buffer.
If MESSAGEBUF is not nil then MSG is also written in message buffer."
  (with-current-buffer (get-buffer-create pico-dashboard-buffer-name)
    (goto-char (point-max))
    (let (buffer-read-only) (insert msg))))

(defun pico-dashboard-insert-page-break ()
  "Insert a page break line in pico-dashboard buffer."
  (pico-dashboard-append pico-dashboard-page-separator))

(defun pico-dashboard-center-line-spaces (line)
  "Centre a LINE according to it's size.
Line can be number which should be a string width or string."
  (let ((width (if (stringp line) (string-width line) line)))
    (make-string (max 0 (floor (/ (- pico-dashboard-banner-length
                                     (+ width 1)) 2))) ?\ )))

;;;; banner

(defun pico-dashboard-insert-ascii-banner-centered (file)
  "Insert banner from FILE."
  (let ((ascii-banner
         (with-temp-buffer
           (insert-file-contents file)
           (let ((banner-width 0))
             (while (not (eobp))
               (let ((line-length (- (line-end-position) (line-beginning-position))))
                 (if (< banner-width line-length)
                     (setq banner-width line-length)))
               (forward-line 1))
             (goto-char 0)
             (let ((margin
                    (max 0 (floor (/ (- pico-dashboard-banner-length banner-width) 2)))))
               (while (not (eobp))
                 (insert (make-string margin ?\ ))
                 (forward-line 1))))
           (buffer-string))))
    (put-text-property 0 (length ascii-banner) 'face 'pico-dashboard-text-banner ascii-banner)
    (insert ascii-banner)))

(defun pico-dashboard--type-is-gif-p (image-path)
  "Return if image is a gif.
String -> bool.
Argument IMAGE-PATH path to the image."
  (eq 'gif (image-type image-path)))

(defun pico-dashboard-insert-image-banner (banner)
  "Display an image BANNER."
  (when (file-exists-p banner)
    (let* ((title pico-dashboard-banner-logo-title)
           (size-props
            (append (when (> pico-dashboard-image-banner-max-width 0)
                      (list :max-width pico-dashboard-image-banner-max-width))
                    (when (> pico-dashboard-image-banner-max-height 0)
                      (list :max-height pico-dashboard-image-banner-max-height))))
           (spec
            (cond ((pico-dashboard--type-is-gif-p banner)
                   (create-image banner))
                  ((image-type-available-p 'imagemagick)
                   (apply 'create-image banner 'imagemagick nil size-props))
                  (t
                   (apply 'create-image banner nil nil
                          (when (and (fboundp 'image-transforms-p)
                                     (memq 'scale (funcall 'image-transforms-p)))
                            size-props)))))
           ;; TODO: For some reason, `elisp-lint' is reporting error void
           ;; function `image-size'.
           (size (when (fboundp 'image-size) (image-size spec)))
           (width (car size))
           (left-margin (max 0 (floor (- pico-dashboard-banner-length width) 2))))
      (goto-char (point-min))
      (insert "\n")
      (insert (make-string left-margin ?\ ))
      (insert-image spec)
      (when (pico-dashboard--type-is-gif-p banner) (image-animate spec 0 t))
      (insert "\n\n")
      (when title
        (insert (pico-dashboard-center-line-spaces title))
        (insert (format "%s\n\n" (propertize title 'face 'pico-dashboard-banner-logo-title)))))))

(defun pico-dashboard-get-banner-path (index)
  "Return the full path to banner with index INDEX."
  (concat pico-dashboard-banners-directory (format "%d.txt" index)))

(defun pico-dashboard-choose-banner ()
  "Return the full path of a banner based on the dotfile value."
  (when pico-dashboard-startup-banner
    (cond ((eq 'classic pico-dashboard-startup-banner)
           (if (and (display-graphic-p) (image-type-available-p 'png))
               pico-dashboard-banner-classic-png
             (pico-dashboard-get-banner-path 1)))
          ((eq 'logo pico-dashboard-startup-banner)
           (if (and (display-graphic-p) (image-type-available-p 'png))
               pico-dashboard-banner-logo-png
             (pico-dashboard-get-banner-path 1)))
          ((eq 'ascii pico-dashboard-startup-banner)
           (pico-dashboard-get-banner-path 1))
          ((integerp pico-dashboard-startup-banner)
           (pico-dashboard-get-banner-path pico-dashboard-startup-banner))
          ((stringp pico-dashboard-startup-banner)
           (if (and (file-exists-p pico-dashboard-startup-banner)
                    (or (string-suffix-p ".txt" pico-dashboard-startup-banner)
                        (and (display-graphic-p)
                             (image-type-available-p (intern (file-name-extension
                                                              pico-dashboard-startup-banner))))))
               pico-dashboard-startup-banner
             (message "could not find banner %s, use default instead" pico-dashboard-startup-banner)
             (pico-dashboard-get-banner-path 1)))
          (t (pico-dashboard-get-banner-path 1)))))

(defun pico-dashboard-insert-banner ()
  "Insert Banner at the top of the pico-dashboard."
  (goto-char (point-max))
  (let ((banner (pico-dashboard-choose-banner)) buffer-read-only)
    (when banner
      (if (image-type-available-p (intern (file-name-extension banner)))
          (pico-dashboard-insert-image-banner banner)
        (pico-dashboard-insert-ascii-banner-centered banner)))))

;;;; items

(defun pico-dashboard-longest-item-length ()
  (let ((len (mapcar (lambda (item)
                       (string-width (pico-dashboard-item-string item)))
                     pico-dashboard-items)))
    (apply #'max len)))

(defun pico-dashboard-item-string (item)
  (let* ((key (plist-get item :key))
         (des (plist-get item :description))
         (fun (plist-get item :function))
         key-string item-string)
    (when key (setq key-string (pico-dashboard-wrap-key key)))
    (setq item-string (concat des pico-dashboard-item-key-separator key-string))))

(defun pico-dashboard-wrap-key (key)
  (let ((beg (nth 0 pico-dashboard-key-wrappers))
        (end (nth 1 pico-dashboard-key-wrappers)))
    (format "%s%s%s" beg key end)))

(defun pico-dashboard-insert-items ()
  "Insert items"
  (let ((max-item-len (pico-dashboard-longest-item-length)))
    (dolist (item pico-dashboard-items)
      (let* ((key (plist-get item :key))
             (des (plist-get item :description))
             (fun (plist-get item :function))
             (key-string (when key (pico-dashboard-wrap-key key)))
             (item-string (concat des pico-dashboard-item-key-separator key-string)))
        (insert (pico-dashboard-center-line-spaces (if pico-dashboard-items-justify
                                                  max-item-len
                                                item-string)))
        (when des
          (insert (propertize des 'face 'pico-dashboard-items))
          (when key
            (when pico-dashboard-items-justify
              (insert (make-string (- max-item-len (string-width item-string))
                                   ?\ )))
            (insert pico-dashboard-item-key-separator)))
        (when key
          (define-key pico-dashboard-mode-map (kbd key) fun)
          (insert (propertize key-string 'face 'pico-dashboard-keys)))
        (insert pico-dashboard-items-separator)))))

;;;; footer

(defun pico-dashboard-random-footer ()
  "Return a random footer from `pico-dashboard-footer-messages'."
  (nth (random (length pico-dashboard-footer-messages)) pico-dashboard-footer-messages))

(defun pico-dashboard-insert-footer ()
  "Insert footer of pico-dashboard."
  (when-let ((footer (and pico-dashboard-set-footer (pico-dashboard-random-footer))))
    (insert "\n")
    (insert (pico-dashboard-center-line-spaces footer))
    (insert " ")
    (insert (propertize footer 'face 'pico-dashboard-footer))
    (insert "\n")))

;;; Footer:

(provide 'pico-dashboard-widgets)

;;; pico-dashboard-widgets.el ends here
