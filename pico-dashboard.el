;;; pico-dashboard.el --- Minimalistic startup screen -*- lexical-binding: t; -*-

;; Copyright (C) 2022 Rafał Rączka <info@rafalraczka.com>

;; Author: Rafał Rączka <info@rafalraczka.com>
;; URL: https://github.com/emacs-pico-dashboard/emacs-pico-dashboard

;; This file is not part of GNU Emacs.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;; Package-Version: 0.0.1-SNAPSHOT
;; Keywords: startup, screen, tools, dashboard
;; Package-Requires: ((emacs "26.1"))

;;; Commentary:

;; Minimal Emacs dashboard.

;;; Code:

(require 'pico-dashboard-widgets)

(defgroup pico-dashboard nil
  "Extensible startup screen."
  :group 'applications)

;; Custom splash screen
(defvar pico-dashboard-mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map (kbd "g") 'pico-dashboard-refresh-buffer)
    map)
  "Default keymap for pico-dashboard mode.")

(defcustom pico-dashboard-after-initialize-hook nil
  "Hook that is run after pico-dashboard buffer is initialized."
  :group 'pico-dashboard
  :type 'hook)

(define-derived-mode pico-dashboard-mode special-mode "Pico-Dashboard"
  "Pico-Dashboard major mode for startup screen."
  :group 'pico-dashboard
  :syntax-table nil
  :abbrev-table nil
  (buffer-disable-undo)
  (when (featurep 'whitespace) (whitespace-mode -1))
  (when (featurep 'linum) (linum-mode -1))
  (when (featurep 'display-line-numbers) (display-line-numbers-mode -1))
  (when (featurep 'page-break-lines) (page-break-lines-mode 1))
  (setq-local revert-buffer-function #'pico-dashboard-refresh-buffer)
  (setq inhibit-startup-screen t
        buffer-read-only t
        truncate-lines t))

(defcustom pico-dashboard-center-content nil
  "Whether to center content within the window."
  :type 'boolean
  :group 'pico-dashboard)

(defconst pico-dashboard-buffer-name "*dashboard*"
  "Pico-Dashboard's buffer name.")

(defvar pico-dashboard-force-refresh nil
  "If non-nil, force refresh pico-dashboard buffer.")

;;;; Insertion

(defun pico-dashboard-insert-startupify-lists ()
  "Insert the list of widgets into the buffer."
  (interactive)
  (when (or pico-dashboard-force-refresh
            (not (eq pico-dashboard-buffer-last-width (window-width))))
    (setq pico-dashboard-banner-length (window-width)
          pico-dashboard-buffer-last-width pico-dashboard-banner-length)
    (with-current-buffer (get-buffer-create pico-dashboard-buffer-name)
      (let (buffer-read-only)
        (erase-buffer)
        (pico-dashboard-insert-banner)
        (pico-dashboard-insert-page-break)
        (pico-dashboard-insert-items)
        (pico-dashboard-insert-page-break)
        (pico-dashboard-insert-footer))
      (goto-char (point-min))
      (pico-dashboard-mode))))

(add-hook 'window-setup-hook
          (lambda ()
            ;; 100 means `pico-dashboard-resize-on-hook' will run last
            (add-hook 'window-size-change-functions 'pico-dashboard-resize-on-hook 100)
            (pico-dashboard-resize-on-hook)))

(defun pico-dashboard-refresh-buffer (&rest _)
  "Refresh buffer."
  (interactive)
  (let ((pico-dashboard-force-refresh t)) (pico-dashboard-insert-startupify-lists))
  (switch-to-buffer pico-dashboard-buffer-name))

(defun pico-dashboard-resize-on-hook (&optional _)
  "Re-render pico-dashboard on window size change."
  (let ((space-win (get-buffer-window pico-dashboard-buffer-name))
        (frame-win (frame-selected-window)))
    (when (and space-win
               (not (window-minibuffer-p frame-win)))
      (with-selected-window space-win
        (pico-dashboard-insert-startupify-lists)))))

;;;###autoload
(defun pico-dashboard-setup-startup-hook ()
  "Setup post initialization hooks.
If a command line argument is provided, assume a filename and skip displaying
Pico-Dashboard."
  (when (< (length command-line-args) 2)
    (add-hook 'after-init-hook #'pico-dashboard-insert-startupify-lists)
    (add-hook 'emacs-startup-hook (lambda ()
                                    (switch-to-buffer pico-dashboard-buffer-name)
                                    (goto-char (point-min))
                                    (redisplay)
                                    (run-hooks 'pico-dashboard-after-initialize-hook)))))

;;;; Footer:

(provide 'pico-dashboard)

;;; pico-dashboard.el ends here
